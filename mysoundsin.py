import math
from mysound import Sound


class SoundSin (Sound):

    def __init__(self, duration, frequency, amplitude):
        """
        "Inicializa una nueva instancia de la clase
        SoundSin con la duración, frecuencia y amplitud especificadas.

        :param duration: La duración del sonido en segundos
        :param frequency: La frecuencia de la onda sinusoidal en Hz
        :param amplitude: La amplitud de la onda sinusoidal
        :return: Ninguno (No devuelve un valor)"
        """
        # Llama al constructor de la clase base (Sound) para
        # inicializar la duración
        super().__init__(duration)

        # Crea una señal sinusoidal y almacénala en el buffer
        self.sin(frequency, amplitude)
